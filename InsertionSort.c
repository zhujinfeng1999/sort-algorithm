#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 10

void generate(int array[], const int length, const int min, const int max);
void print(const int array[], const int length);
void swap(int *a, int *b);
void insertionSort(int array[], const int length);

int main()
{
	int array[] = { 5, 4, 3, 2, 1 }, length = 5;
	int min = 1, max = 100;

	// generate(array, length, min, max);

	printf("Before:\t");
	print(array, length);

	insertionSort(array, length);

	printf("After:\t");
	print(array, length);

	return 0;
}

void generate(int array[], const int length, const int min, const int max)
{
	srand((unsigned) time(NULL));
	
	for (int i = 0; i < length; i++)
	{
		// Random min -> max
		array[i] = rand() % (max - min + 1) + min;
	}
}

void print(const int array[], const int length)
{
	for (int i = 0; i < length; i++)
	{
		printf("%4d", array[i]);
	}
	printf("\n");
}

void swap(int *a, int *b)
{
	int c = *a;
	*a = *b;
	*b = c;
}

void insertionSort(int array[], const int length)
{
	for (int i = 1; i < length; i++)
	{
		int cache = array[i];
		int j = i - 1;

		while (array[j] > cache && j >= 0)
		{
			array[j + 1] = array[j];
			j--;
		}

		array[j + 1] = cache;
	}
}
