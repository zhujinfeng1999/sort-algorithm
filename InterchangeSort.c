#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 10

void generate(int array[], const int length, const int min, const int max);
void print(const int array[], const int length);
void swap(int *a, int *b);
void interchangeSort(int array[], const int length);

int main()
{
	int array[MAX], length = 10;
	int min = 1, max = 100;

	generate(array, length, min, max);

	printf("Before:\t");
	print(array, length);

	interchangeSort(array, length);

	printf("After:\t");
	print(array, length);

	return 0;
}

void generate(int array[], const int length, const int min, const int max)
{
	srand((unsigned) time(NULL));
	
	for (int i = 0; i < length; i++)
	{
		// Random min -> max
		array[i] = rand() % (max - min + 1) + min;
	}
}

void print(const int array[], const int length)
{
	for (int i = 0; i < length; i++)
	{
		printf("%4d", array[i]);
	}
	printf("\n");
}

void swap(int *a, int *b)
{
	int c = *a;
	*a = *b;
	*b = c;
}

void interchangeSort(int array[], const int length)
{
	for (int i = 0; i < length - 1; i++)
	{
		for (int j = i + 1; j < length; j++)
		{
			if (array[i] > array[j])
			{
				swap(&array[i], &array[j]);
			}
		}
	}
}
