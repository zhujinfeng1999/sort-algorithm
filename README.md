# Tổng hợp các thuật toán sắp xếp
Lưu trữ các thuật toán sắp xếp để phục vụ nhu cầu học tập, tham khảo,...

## Mục lục:
- [Swap (Hoán vị)](https://gitlab.com/zhujinfeng1999/sort-algorithm#swap-ho%C3%A1n-v%E1%BB%8B)
- [Interchange Sort (Đổi chỗ trực tiếp)](https://gitlab.com/zhujinfeng1999/sort-algorithm#interchange-sort-%C4%91%E1%BB%95i-ch%E1%BB%97-tr%E1%BB%B1c-ti%E1%BA%BFp)
- [Bubble Sort (Sắp xếp nổi bọt)](https://gitlab.com/zhujinfeng1999/sort-algorithm#bubble-sort-s%E1%BA%AFp-x%E1%BA%BFp-n%E1%BB%95i-b%E1%BB%8Dt)
- [Selection Sort (Sắp xếp chèn)](https://gitlab.com/zhujinfeng1999/sort-algorithm#selection-sort-s%E1%BA%AFp-x%E1%BA%BFp-ch%E1%BB%8Dn)

## Swap (Hoán vị)
**Code (C):**
```c
void swap(int *a, int *b)
{
    int c = *a;
    *a = *b;
    *b = c;
}
```

## Interchange Sort (Đổi chỗ trực tiếp)
**Mô tả thuật toán:**
Xuất phát từ đầu dãy, lần lượt tìm những phần tử còn lại không thỏa thứ tự với phần tử đang xét. Với mỗi phần tử tìm được mà không thỏa thứ tự, thực hiện hoán vị để thỏa thứ tự. Lặp lại tương tự với các phần tử tiếp theo.

**Các bước thực hiện:**
```
- B1: i = 0
- B2: j = i + 1
- B3:
    Nếu j < length:
        Nếu array[i] > array[j]:
            Đổi chỗ array[i] và array[j]
            j = j + 1
- B4:
    Nếu i < length - 1:
        Lặp lại bước 2
    Ngược lại:
        Kết thúc
```

**Mã giả:**
```
for i = 0 to length - 1, i + 1:
    for j = i + 1 to length, j + 1:
        if array[i] > array[j]:
            swap array[i] array[j]
```

**Code (C):**
```c
void interchangeSort(int array[], const int length)
{
    for (int i = 0; i < length - 1; i++)
    {
        for (int j = i + 1; j < length; j++)
        {
            swap(&array[i], &array[j]);
        }
    }
}
```

## Bubble Sort (Sắp xếp nổi bọt)
**Mô tả thuật toán:**
Xuất phát từ phần tử cuối danh sách ta tiến hành so sánh với phần tử bên trái của nó. Nếu phần tử đang xét có khóa nhỏ hơn phần tử bên trái của nó ta tiến đưa nó về bên trái của dãy bằng cách hoán vị với phần tử bên trái của nó. Tiếp tục thực hiện như thế đối với bài toán có n phần tử thì sau n – 1 bước ta thu được danh sách tăng dần.

**Các bước thực hiện:**
```
- B1: i = 0
- B2: j = n - 1
    Nếu j > i:
        Nếu array[j - 1] > array[j]:
            Đổi chỗ array[j - 1] và array[j]
            j = j - 1
- B3: i = i + 1
    Nếu i > n - 2:
        Kết thúc
    Ngược lại:
        Lặp lại bước 2
```

**Mã giả:**
```
for i = 0 to length - 1, i + 1:
    for j = length - 1, j > i, j - 1:
        if array[j - 1] > array[j]:
            swap array[j - 1] array[j]
```

**Code (C):**
```c
void bubbleSort(int array[], const int length)
{
	for (int i = 0; i < length - 1; i++)
	{
		for (int j = length - 1; j > i; j--)
		{
			if (array[j - 1] > array[j])
			{
				swap(&array[j - 1], &array[j]);
			}
		}
	}
}
```

## Selection Sort (Sắp xếp chọn)
**Mô tả thuật toán:**
Giải thuật sắp xếp này là một giải thuật dựa trên việc so sánh in-place, trong đó danh sách được chia thành hai phần, phần được sắp xếp (sorted list) ở bên trái và phần chưa được sắp xếp (unsorted list) ở bên phải. Ban đầu, phần được sắp xếp là trống và phần chưa được sắp xếp là toàn bộ danh sách ban đầu.

Phần tử nhỏ nhất được lựa chọn từ mảng chưa được sắp xếp và được tráo đổi với phần bên trái nhất và phần tử đó trở thành phần tử của mảng được sắp xếp. Tiến trình này tiếp tục cho tới khi toàn bộ từng phần tử trong mảng chưa được sắp xếp đều được di chuyển sang mảng đã được sắp xếp.

Giải thuật này không phù hợp với tập dữ liệu lớn khi mà độ phức tạp trường hợp xấu nhất và trường hợp trung bình là O(n^2) với n là số phần tử.

**Các bước thực hiện:**
```
- B1: i = 0, min = i
- B2: Tìm phần tử nhỏ nhất trong dãy từ array[i + 1] ... array[length - 1]. Nếu tìm được thì gán min = vị trí của phần tử nhỏ nhất
- B3:
    Nếu i != min:
        Hoán vị array[i] và array[min]
- B4:
    Nếu i < n - 2:
        i = i + 1
        Lặp lại bước 2
    Ngược lại:
        Kết thúc
```

**Mã giả:**
```
for i = 0 to length - 2, i + 1:
    set min = i
    for j = i + 1 to length - 1, j + 1:
        if array[min] > array[j]:
            set min = j
            
    if i != min:
        swap array[i] array[min]
```

**Code (C):**
```c
void selectionSort(int array[], const int length)
{
	for (int i = 0; i < length - 1; i++)
	{
		int min = i;
		for (int j = i + 1; j < length; j++)
		{
			if (array[min] > array[j])
			{
				min = j;
			}
		}
		if (i != min)
		{
			swap(&array[i], &array[min]);
		}
	}
}
```
